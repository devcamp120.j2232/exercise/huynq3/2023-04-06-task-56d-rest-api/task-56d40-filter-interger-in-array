package com.devcamp.arrayfilterinputapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;
@Service
public class IntegerFilter {
    private int[] rainbows = {1, 23, 12, 43, 19, 65, 74, 14, 15, 28};
    public ArrayList<Integer> searchIntegers(int pos){
        ArrayList<Integer> integerFilter = new ArrayList<>();
        for (int integer: rainbows){
            if (integer > pos){
                integerFilter.add(integer);
            }
        }
        return integerFilter;
    }
    public int getInteger(int index){
        int integer = -1;
        if (index >=0 && index <=10){
            integer = rainbows[index];
        }
        return integer;
    }
}
